package api;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Api {

    public static String getTranslation(String text) throws Exception {
        if (text.equals("")) {
            return "Вы не ввели слово";
        }

        String link = "https://translate.google.com/m?sl=en&tl=ru&q=" + URLEncoder.encode(text, StandardCharsets.UTF_8) + "&hl=ru";
        Document doc = Jsoup.connect(link).get();
        Elements element = doc.getElementsByClass("result-container");
        return new String(element.text().getBytes(StandardCharsets.UTF_8), Charset.forName("CP1251"));
    }
}
