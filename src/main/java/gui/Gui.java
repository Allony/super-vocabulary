package gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import api.Api;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import background.Back;
import constants.Constants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class Gui extends Application {
    public static JSONArray generalBase;

    @Override
    public void start(Stage stage) {
        // Context
        MenuItem searchItem = new MenuItem("search");
        searchItem.setAccelerator(KeyCombination.keyCombination("Ctrl+F"));

        MenuItem deleteWord = new MenuItem("Delete word");
        deleteWord.setAccelerator(KeyCombination.keyCombination("Ctrl+D"));

        ContextMenu search = new ContextMenu(searchItem, deleteWord);

        MenuItem addItem = new MenuItem("Add word");
        addItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        ContextMenu addWord = new ContextMenu();
        addWord.getItems().add(addItem);

        TextField searchField = new TextField();

        // Header children
        TextField enText = new TextField();

        enText.setPromptText("set text to translate");
        enText.setContextMenu(addWord);
        enText.setCursor(Cursor.DEFAULT);

        Button translate = new Button("translate");

        TextField ruText = new TextField();
        ruText.setPromptText("translated");
        ruText.setContextMenu(addWord);
        ruText.setCursor(Cursor.DEFAULT);

        translate.setOnAction(actionEvent -> {
            try {
                ruText.setText(new String(Api.getTranslation(enText.getText().trim()).getBytes(), StandardCharsets.UTF_8).trim());
            } catch (Exception e) {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setHeaderText("Conection error");
                error.setContentText("Check your internet connection");
                error.show();
                e.printStackTrace();
            }
        });

        enText.textProperty().addListener((observableValue, s, t1) -> {
            if (!s.equals(t1)) {
                ruText.clear();
            }
        });

        // Header pane
        FlowPane headerPane = new FlowPane(20, 0);
        headerPane.setOrientation(Orientation.HORIZONTAL);
        headerPane.setPadding(new Insets(10, 5, 10, 10));
        headerPane.setAlignment(Pos.TOP_CENTER);
        headerPane.getChildren().add(enText);
        headerPane.getChildren().add(translate);
        headerPane.getChildren().add(ruText);

        // Body children
        TextArea rareArea = new TextArea();
        rareArea.setEditable(false);
        rareArea.setContextMenu(search);
        rareArea.setPrefHeight(Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);

        ScrollPane rareScroll = new ScrollPane(rareArea);

        TitledPane rare = new TitledPane();
        rare.setText("Rare");
        rare.setContent(rareScroll);

        TextArea oftenArea = new TextArea();
        oftenArea.setEditable(false);
        oftenArea.setContextMenu(search);
        oftenArea.setPrefHeight(Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);

        ScrollPane oftenScroll = new ScrollPane(oftenArea);

        TitledPane often = new TitledPane();
        often.setText("Repeat periodically");
        often.setContent(oftenScroll);

        TextArea alwaysArea = new TextArea();
        alwaysArea.setEditable(false);
        alwaysArea.setContextMenu(search);
        alwaysArea.setPrefHeight(Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);

        ScrollPane alwaysScroll = new ScrollPane(alwaysArea);

        TitledPane always = new TitledPane();
        always.setText("Common");
        always.setContent(alwaysScroll);

        Accordion list = new Accordion();
        list.setContextMenu(search);
        list.getPanes().add(rare);
        list.getPanes().add(often);
        list.getPanes().add(always);
        list.setExpandedPane(rare);

        // Body pane
        FlowPane bodyPane = new FlowPane();
        bodyPane.getChildren().add(list);
        bodyPane.setAlignment(Pos.BOTTOM_CENTER);

        // Setting words
        Back.setWords(rareArea, oftenArea, alwaysArea, Constants.LOAD_FROM_FILE);

        // Main pane
        VBox mainPane = new VBox();

        mainPane.getChildren().add(headerPane);
        mainPane.getChildren().add(bodyPane);

        ScrollPane scroll = new ScrollPane(mainPane);
        scroll.setFitToWidth(true);
        Scene mainScene = new Scene(scroll);
        stage.setTitle("Super dictionary");
        stage.setScene(mainScene);
        stage.show();

        // Context events
        searchItem.setOnAction(actionEvent -> {
            if (mainPane.getChildren().size() == 2) {
                mainPane.getChildren().add(searchField);
            }
            searchField.setPromptText("Search...");
        });

        addWord.setOnAction(actionEvent -> {
            String word = enText.getText();
            String translation = ruText.getText();
            for (int i = 0; i < generalBase.length(); i++) {
                if (generalBase.getJSONObject(i).get("Word").equals(word)) {
                    generalBase.getJSONObject(i).put("Value", (generalBase.getJSONObject(i).getInt("Value") + 1));
                    Alert message = new Alert(Alert.AlertType.ERROR);
                    message.setHeaderText("Can't add a word");
                    message.setContentText("Word is already exist");
                    message.show();
                    return;
                } else if (translation.isEmpty()) {
                    Alert message = new Alert(Alert.AlertType.ERROR);
                    message.setHeaderText("Can't add a word");
                    message.setContentText("Fields are empty");
                    message.show();
                    return;
                }
            }
            rareArea.appendText(word + " - " + translation + '\n');
            generalBase.put(new JSONObject().put("Word", word)
                    .put("Translate", translation).put("Value", 0));
        });

        searchField.setOnKeyPressed(keyEvent -> {

            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                String word = searchField.getText();
                if (searchField.getPromptText().equals("Search...")) {
                    Alert message = new Alert(Alert.AlertType.INFORMATION);

                    for (int i = 0; i < generalBase.length(); i++) {
                        if (generalBase.getJSONObject(i).get("Word").equals(word)) {
                            generalBase.getJSONObject(i).put("Value", generalBase.getJSONObject(i).getInt("Value") + 1);
                            message.setContentText(word + " - " + generalBase.getJSONObject(i).get("Translate"));
                            message.show();
                            mainPane.getChildren().remove(searchField);
                            searchField.clear();
                        }
                    }
                } else {
                    for (int i = 0; i < generalBase.length(); i++) {
                        if (generalBase.getJSONObject(i).get("Word").equals(word)) {
                            generalBase.remove(i);
                            rareArea.clear();
                            oftenArea.clear();
                            alwaysArea.clear();
                            Back.setWords(rareArea, oftenArea, alwaysArea, Constants.UPDATE);
                            mainPane.getChildren().remove(searchField);
                            searchField.clear();
                        }
                    }
                }

            } else if (keyEvent.getCode().equals(KeyCode.ESCAPE)) {
                mainPane.getChildren().remove(searchField);
            }
        });

        deleteWord.setOnAction(actionEvent -> {
            if (mainPane.getChildren().size() == 2) {
                mainPane.getChildren().add(searchField);
            }
            searchField.setPromptText("Set text to delete...");

        });
    }

    @Override
    public void stop() throws Exception {
        FileWriter fileWriter = new FileWriter("Dictionary.json");
        fileWriter.write(generalBase.toString() + '\n');
        fileWriter.flush();
        fileWriter.close();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
