package background;

import javafx.scene.control.TextArea;
import constants.Constants;
import gui.Gui;
import org.json.JSONArray;

import java.io.*;

public class Back {

    public static void setWords(TextArea rare, TextArea often, TextArea always, String update) {
        try {
            if (update.equals(Constants.LOAD_FROM_FILE))
                Gui.generalBase = load();

            for (int a = 0; a < Gui.generalBase.length(); a++) {
                int value = Gui.generalBase.getJSONObject(a).getInt("Value");
                if (value < 2) {
                    rare.appendText(Gui.generalBase.getJSONObject(a).getString("Word") + " - " +
                            Gui.generalBase.getJSONObject(a).get("Translate") + '\n');
                } else if (value <= 6) {
                    often.appendText(Gui.generalBase.getJSONObject(a).get("Word") + " - " +
                            Gui.generalBase.getJSONObject(a).get("Translate") + '\n');
                } else {
                    always.appendText(Gui.generalBase.getJSONObject(a).get("Word") + " - " +
                            Gui.generalBase.getJSONObject(a).get("Translate") + '\n');
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static JSONArray load() throws IOException {
        File test = new File("Dictionary.json");

        if (!test.exists()) {
            try {
                FileWriter createFile = new FileWriter("Dictionary.json");
                createFile.write("[]");
                createFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String buffer;
        String res = "";
        FileReader readFile = new FileReader(test);
        BufferedReader readFileFlow = new BufferedReader(readFile);

        if ((buffer = readFileFlow.readLine()) != null)
            res += buffer;

        return new JSONArray(res);
    }

}
